import 'package:flutter/material.dart';
import 'package:flutter_theme/text_theme.dart';

class AppTheme {
  static get darkTheme => ThemeData(
        primarySwatch: Colors.grey,
        brightness: Brightness.dark,
        textTheme: textDarkTheme,
        scaffoldBackgroundColor: Colors.red,
        colorScheme: const ColorScheme.dark(
            //You can provide different color here
            ),
      );

  static get lightTheme => ThemeData(
        primarySwatch: Colors.grey,
        brightness: Brightness.light,
        scaffoldBackgroundColor: Colors.white,
        textTheme: textLightTheme,
        colorScheme: const ColorScheme.light(
            //You can provide different color here
            ),
      );
}
