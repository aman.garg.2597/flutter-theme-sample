import 'package:flutter/material.dart';

class ThemeChangeNotifier with ChangeNotifier {
  ThemeMode _themeMode;

  ThemeChangeNotifier(this._themeMode);

  getThemeMode() => _themeMode;

  void setThemeMode(ThemeMode mode) {
    _themeMode = mode;
    notifyListeners();
  }
}
