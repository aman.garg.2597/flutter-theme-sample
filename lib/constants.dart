class KPrefs {
  static const appTheme = 'Theme';
  static const themeDark = 'Dark';
  static const themeLight = 'Light';
  static const themeSystemDefault = 'System default';
}
