import 'package:flutter/material.dart';

import 'app_colors.dart';

TextTheme textLightTheme = const TextTheme(
  headline1: TextStyle(
    fontSize: 36,
    fontWeight: FontWeight.bold,
    color: AppColors.black,
  ),
  headline2: TextStyle(
    fontSize: 28,
    fontWeight: FontWeight.w400,
    color: AppColors.black,
  ),
  headline3: TextStyle(
    fontSize: 20,
    fontWeight: FontWeight.normal,
    color: AppColors.black,
  ),
  subtitle1: TextStyle(
    fontSize: 14,
    fontWeight: FontWeight.normal,
  ),
);

TextTheme textDarkTheme = const TextTheme(
  headline1: TextStyle(
    fontSize: 36,
    fontWeight: FontWeight.bold,
    color: AppColors.white70,
  ),
  headline2: TextStyle(
    fontSize: 28,
    fontWeight: FontWeight.w400,
    color: AppColors.white70,
  ),
  headline3: TextStyle(
    fontSize: 20,
    fontWeight: FontWeight.normal,
    color: AppColors.white70,
  ),
  subtitle1: TextStyle(
    fontSize: 14,
    fontWeight: FontWeight.normal,
  ),
);
