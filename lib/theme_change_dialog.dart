import 'package:flutter/material.dart';
import 'package:flutter_theme/constants.dart';
import 'package:flutter_theme/pref_utils.dart';
import 'package:flutter_theme/theme_change_notifier.dart';
import 'package:provider/provider.dart';

import 'app_colors.dart';

class ThemeChangeDialog extends StatefulWidget {
  const ThemeChangeDialog({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _ThemeChangeDialogState();
}

class _ThemeChangeDialogState extends State<ThemeChangeDialog> {
  int _selectedPosition = -1;
  late List<ThemeValue> themes;

  @override
  Widget build(BuildContext context) {
    themes = ThemeValue.getThemeList(
        Provider.of<ThemeChangeNotifier>(context, listen: false).getThemeMode());
    _selectedPosition = themes.indexWhere((element) => element.isSelected);
    return Dialog(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: ListView.builder(
          shrinkWrap: true,
          itemBuilder: (context, position) {
            return ThemeListItemWidget(
              themeValue: themes[position],
              position: position,
              selectedPosition: _selectedPosition,
              onChange: (position) {
                _updateState(position);
              },
            );
          },
          itemCount: themes.length,
        ),
      ),
    );
  }

  _updateState(int position) {
    setState(() {
      _selectedPosition = position;
    });
    onThemeChanged(themes[position]);
    Navigator.pop(context);
  }

  void onThemeChanged(ThemeValue themeValue) async {
    Provider.of<ThemeChangeNotifier>(context, listen: false).setThemeMode(themeValue.themeMode);
    PrefUtils.saveTheme(themeValue.themeName);
  }
}

class ThemeListItemWidget extends StatelessWidget {
  const ThemeListItemWidget(
      {Key? key,
      required this.selectedPosition,
      required this.position,
      required this.themeValue,
      required this.onChange})
      : super(key: key);

  final int selectedPosition, position;
  final ThemeValue themeValue;
  final Function(int) onChange;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      highlightColor: Colors.transparent,
      splashColor: Colors.transparent,
      onTap: () => onChange(position),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Radio(
            value: selectedPosition,
            groupValue: position,
            activeColor: AppColors.redDark,
            onChanged: (_) => onChange(position),
          ),
          Text(themeValue.themeName),
        ],
      ),
    );
  }
}

class ThemeValue {
  final String themeName;
  final ThemeMode themeMode;
  bool isSelected;

  ThemeValue(this.themeName, this.themeMode, this.isSelected);

  static List<ThemeValue> getThemeList(ThemeMode currentThemeMode) {
    return [
      ThemeValue(KPrefs.themeSystemDefault, ThemeMode.system,
          currentThemeMode.name == ThemeMode.system.name),
      ThemeValue(KPrefs.themeDark, ThemeMode.dark, currentThemeMode.name == ThemeMode.dark.name),
      ThemeValue(KPrefs.themeLight, ThemeMode.light, currentThemeMode.name == ThemeMode.light.name),
    ];
  }
}
