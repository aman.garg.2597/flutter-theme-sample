import 'package:flutter_theme/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PrefUtils {
  static SharedPreferences? prefs;

  static Future<bool> init() async {
    prefs = await SharedPreferences.getInstance();
    return prefs != null;
  }

  static Future<bool>? clearPrefs() {
    return prefs?.clear();
  }

  static void saveTheme(String theme) {
    prefs?.setString(KPrefs.appTheme, theme);
  }

  static String getTheme() {
    return prefs?.getString(KPrefs.appTheme) ?? KPrefs.themeSystemDefault;
  }
}
