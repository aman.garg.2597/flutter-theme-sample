import 'package:flutter/material.dart';
import 'package:flutter_theme/app_colors.dart';
import 'package:flutter_theme/theme_change_dialog.dart';
import 'package:flutter_theme/theme_change_notifier.dart';
import 'package:provider/provider.dart';

class SplashPage extends StatelessWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var themeNotifier = Provider.of<ThemeChangeNotifier>(context);
    return Scaffold(
      appBar: AppBar(),
      body: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text('Heading 1 Sample',
              textAlign: TextAlign.center, style: Theme.of(context).textTheme.headline1),
          const SizedBox(height: 20),
          Text('Heading 2 Sample',
              textAlign: TextAlign.center, style: Theme.of(context).textTheme.headline2),
          const SizedBox(height: 20),
          Text('Heading 3 Sample',
              textAlign: TextAlign.center, style: Theme.of(context).textTheme.headline3),
          InkWell(
            onTap: () {
              showDialog(context: context, builder: (context) => const ThemeChangeDialog());
            },
            child: Padding(
              padding: const EdgeInsets.all(20),
              child: Text(
                'Change Theme',
                style: Theme.of(context)
                    .textTheme
                    .subtitle1
                    ?.copyWith(color: Theme.of(context).colorScheme.someCustomColor),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
