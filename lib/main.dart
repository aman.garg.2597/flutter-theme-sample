import 'package:flutter/material.dart';
import 'package:flutter_theme/pref_utils.dart';
import 'package:flutter_theme/splash_page.dart';
import 'package:flutter_theme/theme_change_notifier.dart';
import 'package:flutter_theme/theme_change_provider_widget.dart';
import 'package:provider/provider.dart';

import 'app_theme.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await PrefUtils.init();
  runApp(const ThemeChangeProviderWidget(widget: MyApp()));
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
    listenBrightnessChanges();
  }

  @override
  Widget build(BuildContext context) {
    final themeNotifier = Provider.of<ThemeChangeNotifier>(context);
    return MaterialApp(
      title: 'Flutter Demo',
      theme: AppTheme.lightTheme,
      darkTheme: AppTheme.darkTheme,
      themeMode: themeNotifier.getThemeMode(),
      home: const SplashPage(),
    );
  }

  void listenBrightnessChanges() {
    var window = WidgetsBinding.instance!.window;
    window.onPlatformBrightnessChanged = () {
      WidgetsBinding.instance?.handlePlatformBrightnessChanged();
      // This callback is called every time the brightness changes.
      Provider.of<ThemeChangeNotifier>(context, listen: false).setThemeMode(ThemeMode.system);
    };
  }
}
