import 'package:flutter/material.dart';
import 'package:flutter_theme/constants.dart';
import 'package:flutter_theme/pref_utils.dart';
import 'package:flutter_theme/theme_change_notifier.dart';
import 'package:provider/provider.dart';

class ThemeChangeProviderWidget extends StatelessWidget {
  const ThemeChangeProviderWidget({Key? key, required this.widget}) : super(key: key);
  final Widget widget;

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<ThemeChangeNotifier>(
      create: (BuildContext context) {
        String theme = PrefUtils.getTheme();
        if (theme.isEmpty || theme == KPrefs.themeSystemDefault) {
          PrefUtils.saveTheme(KPrefs.themeSystemDefault);
          return ThemeChangeNotifier(ThemeMode.system);
        }
        return ThemeChangeNotifier(theme == KPrefs.themeDark ? ThemeMode.dark : ThemeMode.light);
      },
      child: widget,
    );
  }
}
