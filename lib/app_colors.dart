import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AppColors extends MaterialColor {
  const AppColors(int primary, Map<int, Color> swatch) : super(primary, swatch);

  static const int primaryColorValue = 0xFFFFFFFF;

  static const primaryColor = Color(0xFFFFFFFF);

  static const colorTransparent = Color(0x00000000);

  //Blue Color
  static const primaryBlue = Color(0xFF082a58);
  static const primaryBlue3 = Color(0xFF0d2b58);
  static const primaryBlue4 = Color(0xFF00214e);
  static const primaryBlue50 = Color(0x80082a58);
  static const primaryBlue20 = Color(0x33082a58);
  static const bluishGrey = Color(0xFFedf1f5);
  static const whiteShade1 = Color(0xFFf7f8fb);
  static const skyBlue = Color(0xFF87b3ee);
  static const primaryDarkBlue = Color(0xFF00214F);
  static const secondaryBlue = Color(0xFF4B4E6C);
  static const midnightBlue = Color(0xFF283153);
  static final overlayBlue = const Color(0xFF2E3238).withOpacity(0.1);
  static const nonSelectableBlue = Color(0xff868A9B);
  static const pageBackground = Color(0xFFF0F2F8);

  static const labelTextColor = Color(0xFF323232);

  //Snackbar color
  static const snackBarColor = Color(0xff424242);
  static const snackBarRed = Color(0xffe53e3f);
  static const snackBarGreen = Color(0xff2dca73);

  //White shades
  static const white = Color(0xFFFFFFFF);
  static const white50 = Color(0x80FFFFFF);
  static const white60 = Color(0x99FFFFFF);
  static const white70 = Color(0xB3FFFFFF);
  static const lightGrey = Color(0xFFefefef);
  static const parrotGreen = Color(0xFFb5f85d);
  static const parrotGreen1 = Color(0xFFbfff6b);
  static const greenColor = Color(0xFF49c27f);
  static const redDark = Color(0xFFd53b3b);
  static const redDark1 = Color(0xFFd40208);
  static const primaryDarkBluishGrey = Color(0xFF131415);
  static const primaryDarkBluishGrey40 = Color(0x66131415);
  static const primaryDarkBluishGrey50 = Color(0x80131415);
  static const primaryDarkBluishGrey20 = Color(0x33131415);
  static const primaryDarkBluishGrey70 = Color(0xB3131415);
  static const greyContainer = Color(0xffd8d8d8);
  static const black1 = Color(0xFF161618);
  static const black = Color(0xFF000000);
  static const black30 = Color(0x4D000000);
  static const black50 = Color(0x80000000);
  static const black60 = Color(0x99000000);
  static const black70 = Color(0xB3000000);
  static const greyLight = Color(0xFFe0e0e0);
  static const greyLight1 = Color(0xFFebebeb);
  static const greyLight2 = Color(0x33a9a9a9);
  static const greyLight3 = Color(0xFFeeeeee);
  static const greyLight4 = Color(0xffb3b3b4);
  static const greyLight5 = Color(0xff909091);
  static const greyMain = Color(0xFF989898);
  static const greySeparator = Color(0xFF979797);
  static const greySeparator2 = Color(0xFFdddddd);
  static const drawerSeparatorColor = Color(0x1F909091);
  static const greySeparator40 = Color(0x66979797);
  static const backgroundColor = Color(0xFFf7f8fb);
  static const lightBlue = Color(0xFF2771d8);
  static const greyishBlue = Color(0xFF6d6d7c);
  static const greyishBlue16 = Color(0x296d6d7c);
  static const darkGrey = Color(0xff212123);

  //Orange shades
  static const orange = Color(0xFFF69220);

  static const MaterialColor primaryColorSwatch = MaterialColor(primaryColorValue, <int, Color>{
    50: Color(primaryColorValue),
    100: Color(primaryColorValue),
    200: Color(primaryColorValue),
    300: Color(primaryColorValue),
    400: Color(primaryColorValue),
    500: Color(primaryColorValue),
    600: Color(primaryColorValue),
    700: Color(primaryColorValue),
    800: Color(primaryColorValue),
    900: Color(primaryColorValue),
  });
}

extension ColorSchemeExtension on ColorScheme {
  Color get someCustomColor =>
      brightness == Brightness.light ? AppColors.darkGrey : AppColors.lightGrey;
}
